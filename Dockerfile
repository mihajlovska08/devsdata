#Build Java App
FROM gradle:jdk8 as builder
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build -x test

#Build & Run Java Jar File
FROM openjdk:8
EXPOSE 8080
COPY --from=builder /home/gradle/src/build/libs/DevsData-1.jar /app/
WORKDIR /app
ENTRYPOINT ["java", "-jar", "DevsData-1.jar"]