package com.DevsData;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DevsDataApplication {

	public static void main(String[] args) {
		SpringApplication.run(DevsDataApplication.class, args);
	}

}
