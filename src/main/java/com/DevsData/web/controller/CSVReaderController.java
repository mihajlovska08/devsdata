package com.DevsData.web.controller;

import com.DevsData.service.CSVReaderService;
import com.DevsData.web.dto.CSVReaderDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class CSVReaderController {

    @Autowired
    CSVReaderService csvReaderService;

    @GetMapping
    public ResponseEntity<List<CSVReaderDto>> readFile(){
        List<CSVReaderDto> records = new ArrayList<>();
        try {
            records = csvReaderService.readFile("data.csv");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(records, HttpStatus.OK);
    }

}
