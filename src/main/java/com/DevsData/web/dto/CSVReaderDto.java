package com.DevsData.web.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class CSVReaderDto {
    private String FirstName;
    private String LastName;
    private String Email;
    private List<String> param1;
    private List<String>  param2;
    private List<String>  param3;
    private List<String>  param4;
    private List<String>  param5;
    private List<String>  param6;
    private List<String>  param7;
    private List<String>  param8;
    private List<String>  param9;
    private List<String>  param0;

}
