package com.DevsData.service.impl;

import com.DevsData.service.CSVReaderService;
import com.DevsData.utils.UnzipFileUtility;
import com.DevsData.web.dto.CSVReaderDto;
import com.google.common.io.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.client.RestTemplate;

import java.io.*;
import java.net.URI;
import java.util.*;

@Service
public class CSVReaderImpl implements CSVReaderService {

    @Override
    public List<CSVReaderDto> readFile(String fileName) throws Exception {
        UnzipFileUtility.unzipFile();
        File file = ResourceUtils.getFile("classpath:" + fileName);

        List<CSVReaderDto> records = new ArrayList<>();
        Scanner scanner = new Scanner(file);
        if(scanner.hasNextLine()){
            scanner.nextLine();
        }

        while (scanner.hasNextLine()) {
            CSVReaderDto csvReaderDto = getRecordFromLine(scanner.nextLine());
            String correctValue =
                    csvReaderDto.getFirstName().toLowerCase().concat(".")
                            .concat(csvReaderDto.getLastName().toLowerCase()).concat("@domain.com");
            if(correctValue.equals(csvReaderDto.getEmail().toLowerCase())){
                sendPost(csvReaderDto);
                records.add(csvReaderDto);
            }
        }

        return records;
    }

    private CSVReaderDto getRecordFromLine(String line) {
        List<String> values = new ArrayList<>();
        try (Scanner rowScanner = new Scanner(line)) {
            rowScanner.useDelimiter(";");
            while (rowScanner.hasNext()) {
                String value = rowScanner.next().replace('"',' ').trim();
                values.add(value);
            }
            return mapCsvReader(values);
        }
    }

    private CSVReaderDto mapCsvReader(List<String> values) {
        CSVReaderDto csvReaderDto = new CSVReaderDto();

        csvReaderDto.setFirstName(values.get(0));
        csvReaderDto.setLastName(values.get(1));
        csvReaderDto.setEmail(values.get(2));
        csvReaderDto.setParam0(Collections.singletonList(values.get(3)));
        csvReaderDto.setParam1(Collections.singletonList(values.get(4)));
        csvReaderDto.setParam2(Collections.singletonList(values.get(5)));
        csvReaderDto.setParam3(Collections.singletonList(values.get(6)));
        csvReaderDto.setParam4(Collections.singletonList(values.get(7)));
        csvReaderDto.setParam5(Collections.singletonList(values.get(8)));
        csvReaderDto.setParam6(Collections.singletonList(values.get(9)));
        csvReaderDto.setParam7(Collections.singletonList(values.get(10)));
        csvReaderDto.setParam8(Collections.singletonList(values.get(11)));
        return csvReaderDto;
    }

    private void sendPost(CSVReaderDto csvReaderDto) throws Exception {

        RestTemplate restTemplate = new RestTemplate();

        final String baseUrl = "http://54.70.230.245:80/";
        URI uri = new URI(baseUrl);

        restTemplate.postForEntity(uri, csvReaderDto, String.class);

    }
}
