package com.DevsData.service;

import com.DevsData.web.dto.CSVReaderDto;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public interface CSVReaderService {
    List<CSVReaderDto> readFile(String filePath) throws Exception;
}
