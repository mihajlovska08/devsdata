# DevsData Home Test

## Prerequisites

In order to run the application, you need to download and install Intellij IDE for desktop. Please download it from the following link: `https://www.jetbrains.com/idea/download/?gclid=Cj0KCQjwvr6EBhDOARIsAPpqUPEefwsmIZBrf_aL62tSB5lcYxyBvFS9pL8YVvigNvalSCqxZcDai1caAqg9EALw_wcB#section=windows`

## Getting started

To run the application, you need to follow the next steps:
- Open IntelliJ
- Import the project
- Run the application
- Open browser and navigate to localhost:8081 .
- You should see the result.

## Architecture

The application at the moment is designed following n-tier architecture design and Single responsibility pattern.

## Folder Structure

The whole application is located in `src`. In the `src/main/java/com.DevsData` package you can see  a list of folders:

- **web** - folder where API endpoints are defined in the controller sub-folder and includes data transfer object. 
- **service** - this folder includes the whole application logic. It contains interfaces and their implementations.
- **utils** - the common classes or constants that are used all over the application. They are divided in two subfolders:
	
